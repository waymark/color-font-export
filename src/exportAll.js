import sketch from 'sketch'

function toVariableName(text) {
    // Remove group hierarchy
    text = text.split('/').pop();
    return '$' + text.toLowerCase().replace(/[^a-zA-Z\d]/g, '-');
}

function parseColor(color) {
    // Remove 'ff' if at 100% alpha
    return color.endsWith('ff') ? color.slice(0, -2) : color;
}

// Font weight in Sketch:
// 3 - Thin
// 5 - Regular
// 6 - Medium
// 9 - Bold
function makeFontBlock(fontFamily, fontWeight, fontStyle) {
    fontWeight = (fontWeight - 1) * 100;
    if (fontWeight < 1) {
        fontWeight = 1;
    } else if (fontWeight > 1000) {
        fontWeight = 1000;
    }

    fontStyle = fontStyle ? `\n    font-style: ${fontStyle};` : '';

    return `
@font-face {
    font-family: '${fontFamily}';${fontStyle}
    font-weight: ${fontWeight};
    src: url('assets/fonts/') format('');
}
`;

}

export default function () {
    let doc = sketch.getSelectedDocument();

    let colors = doc.swatches
        .map(swatch =>
                `${toVariableName(swatch.name)}: ${parseColor(swatch.color)};`)
        .sort((a, b) => a < b ? -1 : a > b ? 1 : 0)
        .join('\n');

    let foundFonts = {};
    sketch
        .find('Text')
        .forEach(text => {
            let { fontFamily, fontStyle, fontWeight } = text.style;
            if (!foundFonts[fontFamily]) {
                foundFonts[fontFamily] = [];
            }

            if (!foundFonts[fontFamily][fontWeight]) {
                foundFonts[fontFamily][fontWeight] = {};
            }

            let style = String(fontStyle);
            if (!foundFonts[fontFamily][fontWeight][style]) {
                foundFonts[fontFamily][fontWeight][style] =
                    makeFontBlock(fontFamily, fontWeight, fontStyle);
            }
        });

    let fonts = '\n';
    for (let family in foundFonts) {
        foundFonts[family].forEach(byWeight => {
            for (let style in byWeight) {
                fonts += byWeight[style];
            }
        });
    }

    sketch.UI.alert('Colors & fonts', colors + fonts);
}
